const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function createElement(name, answer) {
    let div = document.createElement('div');
    div.textContent = name;
    document.body.appendChild(div);

    let div2 = document.createElement('div');
    div2.textContent = JSON.stringify(answer);
    document.body.appendChild(div2);
}

function kata1() {
    let answer = gotCitiesCSV.split(",");
    createElement("Kata 1", answer);
    return (answer);
}
kata1();

function kata2() {
    let answer = bestThing.split(" ");
    createElement("Kata 2", answer);
    return (answer);
}
kata2();

function kata3() {
    let answer = gotCitiesCSV.split(",").join(";");
    createElement("Kata 3", answer);
    return (answer);
}
kata3();

function kata4() {
    let answer = lotrCitiesArray.join(",");
    createElement("Kata 4", answer);
    return (answer);
}
kata4();

function kata5() {
    let answer = lotrCitiesArray.slice(0, 5);
    createElement("Kata 5", answer);
    return (answer);
}
kata5();

function kata6() {
    let answer = lotrCitiesArray.slice(-5);
    createElement("Kata 6", answer);
    return (answer);
}
kata6();

function kata7() {
    let answer = lotrCitiesArray.slice(2, 5);
    createElement("Kata 7", answer);
    return (answer);
}
kata7();

function kata8() {
    let answer = lotrCitiesArray.splice(2, 1);
    createElement("Kata 8", answer);
    return (answer);
}
kata8();

function kata9() {
    let answer = lotrCitiesArray.splice(5, 2);
    createElement("Kata 9", answer);
    return (answer);
}
kata9();

function kata10() {
    let answer = lotrCitiesArray.splice(2, 0, "Rohan");
    createElement("Kata 10", answer);
    return (answer);
}
kata10();

function kata11() {
    let answer = lotrCitiesArray.splice(5, 1, "Deadest Marshes");
    createElement("Kata 11", answer);
    return (answer);
}
kata11();

function kata12() {
    let answer = bestThing.slice(0, 14);
    createElement("Kata 12", answer);
    return (answer);
}
kata12();

function kata13() {
    let answer = bestThing.slice(-12);
    createElement("Kata 13", answer);
    return (answer);
}
kata13()

function kata14() {
    let answer = bestThing.slice(23, 39);
    createElement("Kata 14", answer);
    return (answer);
}
kata14();

function kata15() {
    let answer = bestThing.substr(-12);
    createElement("Kata 15", answer);
    return (answer);
}
kata15();

function kata16() {
    let answer = bestThing.substr(23, 15);
    createElement("Kata 16", answer);
    return (answer);
}
kata16();

function kata17() {
    let a = bestThing.split(" ");
    let b = a.indexOf("only");
    createElement("Kata 17", b);
    return (b);
}
kata17();

function kata18() {
    let w = bestThing.split(" ");
    let answer = w.indexOf(w[w.length - 1]);
    createElement("Kata 18", answer);
    return (answer);
}
kata18();

function kata19() {

    // const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
    let dv = ["aa", "ee", "ii", "oo", "uu"];

    //I think I worked it out:

    let a = gotCitiesCSV
        .split(",") //turns the gotCities CSV into an array called "a"
        .reduce((pre, cur) => { //pre = [], and cur = each member of "a" (gotCities)
            let b = dv //b is going to be a new array, but will not be output - cur will be output
                .filter(val => cur.indexOf(val) !== -1); //if the str, say 'aa', has an index in say, 'Kings Landing'
            //then b = ['aa'], else b =[]
            //if the str 'ee' has an index in 'Kings Landing'
            //then b = ['aa', 'ee'], or just ['ee'] it 'aa' didn't match
            // else b = [], and so on.
            // we are doing each member of dv against each memeber of gotCities 
            if (b.length > 0) { // if the array b has any members (something matched out filter)
                pre.push(cur); // push the member of gotCities that it matched against to cur
            }
            return pre;
        }, []);

    createElement("Kata 19", a);
    return (a);
}
kata19();

function kata20() {
    let a = lotrCitiesArray.reduce((pre, cur) => {
        if (cur.substr(-2) === "or")
            pre.push(cur);
        return (pre);
    }, [])
    createElement("Kata 20", a);
}
kata20();

function kata21() {
    let a = bestThing.split(" ")
        .filter(val => val.indexOf("b") === 0);
    createElement("Kata 21", a);
}
kata21();

function kata22() {
    let a = 'no';
    for (let i = 0; i < lotrCitiesArray.length; i++) {
        if (lotrCitiesArray[i] === "Mirkwood") {
            a = 'yes';
        }
    }
    createElement("Kata 22", a);
    return (a);
}
kata22();

function kata23() {
    let a = 'no';
    for (i = 0; i < lotrCitiesArray; i++) {
        if (lotrCitiesArray[i] === "Hollywood") {
            a = 'yes'
        }
    }
    createElement("Kata 23", a);
    return (a);
}
kata23();

function kata24() {
    let a = lotrCitiesArray.indexOf('Mirkwood');
    createElement("Kata 24", a);
}
kata24();

function kata25() {
    // a becomes our new array
    let a = lotrCitiesArray
        //if an index of lotrCitiesArray can be split at a space,
        //  this will create a new array
        //  if the length of that new array is >1,
        //  then add the index of lotrArray to the new array a.
        .filter(val => val.split(" ").length > 1);
    createElement("Kata 25", a);
    return (a);
}
kata25();

function kata26() {
    a = lotrCitiesArray.reverse();
    createElement("Kata 26", a);
    return (a);
}
kata26();

function kata27() {
    a = lotrCitiesArray.sort();
    createElement("Kata 27", a);
    return (a);
}
kata27();

function kata28() {
    lotrCitiesArray.sort((a, b) => a.length - b.length);
    createElement("Kata 28", lotrCitiesArray);
    return (lotrCitiesArray);
}
kata28();

function kata29() {
    let answer = lotrCitiesArray.pop();
    createElement("Kata 29", answer);
    return (answer);
}
kata29();

function kata30() {
    lotrCitiesArray.push("Deadest Marshes");
    createElement("Kata 30", lotrCitiesArray);
    return (lotrCitiesArray);
}
kata30();

function kata31() {
    lotrCitiesArray.shift();
    createElement("Kata 31", lotrCitiesArray);
    return (lotrCitiesArray);
}
kata31();

function kata32() {
    lotrCitiesArray.unshift("Rohan");
    createElement("Kata 32", lotrCitiesArray);
    return (lotrCitiesArray);
}
kata32();